-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nodecore, pairs, vector
    = math, minetest, nodecore, pairs, vector
local math_random, math_sqrt
    = math.random, math.sqrt
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local spore = modname .. ":sponge_spore"

minetest.register_node(spore, {
		description = "Sponge Spore",
		tiles = {"nc_tree_humus.png^(nc_sponge.png^[opacity:64)^nc_tree_peat.png"},
		groups = {
			crumbly = 2,
			skyrealm_only = 1
		},
		skyrealm_equiv = {
			"nc_tree:peat",
			"nc_tree:peat",
			"nc_tree:peat",
			"nc_tree:peat",
			"nc_tree:peat",
			"nc_tree:peat",
			"nc_tree:peat",
			"nc_tree:peat"
		},
		sounds = nodecore.sounds("nc_terrain_swishy"),
		mapcolor = {r = 194, g = 182, b = 0},
	})

nodecore.register_craft({
		label = "make sponge spore",
		action = "pummel",
		toolgroups = {thumpy = 2},
		check = api.in_sky_realm,
		nodes = {
			{
				match = {name = "nc_tree:peat", count = 8},
				replace = spore
			}
		}
	})

local alldirs = nodecore.dirs()
minetest.register_abm({
		label = "Sponge Spore Sprouting",
		interval = 10,
		chance = 1,
		nodenames = {spore},
		action = function(pos)
			if not api.in_sky_realm(pos) then return end
			local moist = 0
			local sand = 0
			for _, dir in pairs(alldirs) do
				local pos2 = vector.add(pos, dir)
				local name = minetest.get_node(pos2).name
				sand = sand + minetest.get_item_group(name, "sand")
				moist = moist + minetest.get_item_group(name, "moist")
			end
			if moist < 1 or sand < 1 then return end
			local chance = math_sqrt(moist * sand) / 40
			if math_random() > chance then return end
			nodecore.set_loud(pos, {name = "nc_sponge:sponge_living"})
			return nodecore.witness(pos, "sponge spore sprout")
		end
	})

local skyhint = api.addskyhint()
skyhint("concentrate peat into sponge spore",
	"make sponge spore",
	"nc_tree:peat"
)
skyhint("sprout sponge spore into sponge",
	"sponge spore sprout",
	"make sponge spore"
)
