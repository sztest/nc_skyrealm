-- LUALOCALS < ---------------------------------------------------------
local include, minetest, rawset
    = include, minetest, rawset
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
rawset(_G, modname, {store = minetest.get_mod_storage()})

include("api_effects")
include("api_grid")
include("api_hint")
include("api_portal")
include("api_player")

include("map_barrier")
include("map_treestart")
include("map_islands")

include("rule_aggscorch")
include("rule_dirtygravel")
include("rule_lodeamalgam")
include("rule_spongespore")
include("rule_stonemelt")
include("rule_treespeed")

include("item_restrict")
include("item_falling")

include("skybox")
include("portal")
include("commands")
