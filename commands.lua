-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, nodecore, pairs, tonumber, vector
    = ipairs, minetest, nodecore, pairs, tonumber, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local privname = modname .. "_debug"
minetest.register_privilege(privname, {
		description = "access to debugging commands for skyrealm",
		give_to_admin = true,
		give_to_singleplayer = false
	})

local function showhuds(player)
	local pos = player:get_pos()
	if not api.in_sky_realm(pos) then return end
	for x = -1, 1 do
		for y = -1, 1 do
			for z = -1, 1 do
				local rel = {x = x, y = y, z = z}
				local ipos = api.island_near(pos, rel)
				if ipos then
					local posstr = minetest.pos_to_string(ipos)
					nodecore.hud_set(player, {
							label = modname .. posstr,
							hud_elem_type = "waypoint",
							world_pos = ipos,
							name = "skyblock" .. posstr,
							text = "",
							precision = 1,
							number = 0xffffff,
							ttl = 2
						})
				end
			end
		end
	end
end

local hudkey = modname .. "_hud"
nodecore.interval(1, function()
		for _, player in pairs(minetest.get_connected_players()) do
			if player:get_meta():get_string(hudkey) ~= ""
			and minetest.check_player_privs(player, privname) then
				showhuds(player)
			end
		end
	end)

minetest.register_chatcommand("skyhud", {
		description = "Toggle HUD display of nearby skyblocks",
		privs = {[privname] = true},
		func = function(name)
			local player = minetest.get_player_by_name(name)
			if not player then return false, "Must be logged on" end
			local meta = player:get_meta()
			meta:set_string(hudkey, meta:get_string(hudkey) == "" and "1" or "")
			return true, "Skyblock HUDs now " .. (meta:get_string(hudkey)
				== "" and "OFF" or "ON")
		end
	})

minetest.register_chatcommand("skyset", {
		description = "Set target island for current portal",
		params = "<x> <y> <z>",
		privs = {[privname] = true},
		func = function(name, param)
			local player = minetest.get_player_by_name(name)
			if not player then return false, "Must be logged on" end
			local stack = player:get_wielded_item()
			if (not stack) or stack:get_name() ~= modname .. ":portal" then
				return false, "Must be wielding a portal"
			end

			local parts = param:split(" ")
			if #parts < 3 then return false, "Required params: <x> <y> <z>" end
			local pos = {
				x = tonumber(parts[1]),
				y = tonumber(parts[2]),
				z = tonumber(parts[3]),
			}
			if not (pos.x and pos.y and pos.z) then return false, "Invalid pos" end
			if not api.in_sky_realm(pos) then return false, "Pos not in sky realm" end
			local ipos = api.island_near(pos)
			if not ipos then return false, "No nearby island found" end

			stack:get_meta():set_string(modname, minetest.pos_to_string(ipos))
			player:set_wielded_item(stack)
		end
	})

minetest.register_chatcommand("skyrealm", {
		description = "Enter skyrealm via closest nearby portal",
		privs = {[privname] = true},
		func = function(name)
			local player = minetest.get_player_by_name(name)
			if not player then return false, "Must be logged on" end

			local ppos = player:get_pos()
			if (not ppos) or api.in_sky_realm(ppos) then
				return false, "Cannot use this command in skyrealm"
			end

			local npos, ndist
			for _, pos in ipairs(nodecore.find_nodes_around(ppos, modname .. ":portal", 16)) do
				local diff = vector.subtract(ppos, pos)
				local dist = vector.dot(diff, diff)
				if (not ndist) or (ndist > dist) then
					ndist = dist
					npos = pos
				end
			end
			if not npos then return false, "No nearby portal found" end

			local meta = minetest.get_meta(npos)
			local found = meta:get_string(modname)
			if found == "" then return false, "Portal unassigned" end
			api.player_enter(player, minetest.string_to_pos(found))
		end
	})
