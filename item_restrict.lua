-- LUALOCALS < ---------------------------------------------------------
local ItemStack, minetest, nodecore, pairs, string, table
    = ItemStack, minetest, nodecore, pairs, string, table
local string_format, table_concat
    = string.format, table.concat
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local function ejectequiv(name, qty, pos)
	local def = minetest.registered_items[name] or {}
	local equiv = def.skyrealm_equiv
	if not equiv then return end
	local ejected = {}
	for _, item in pairs(equiv) do
		item = ItemStack(item)
		if qty > 1 then item:set_count(qty * item:get_count()) end
		nodecore.item_eject(pos, item, 5)
		ejected[#ejected + 1] = string_format("%q", item:to_string())
	end
	nodecore.log("action", string_format("skyrealm material %q uncrafted to %s at %s",
			qty > 1 and (name .. " " .. qty) or name,
			table_concat(ejected, ", "), minetest.pos_to_string(pos, 0)))
	return true
end

nodecore.register_aism({
		label = "SkyRealm Restricted Items",
		interval = 1,
		chance = 1,
		itemnames = {"group:skyrealm_only"},
		action = function(stack, data)
			if api.in_sky_realm(data.pos) then return end
			if ejectequiv(stack:get_name(), stack:get_count(), data.pos) then
				return ItemStack("")
			end
		end
	})

minetest.register_abm({
		label = "SkyRealm Restricted Nodes",
		interval = 1,
		chance = 1,
		nodenames = {"group:skyrealm_only"},
		action = function(pos, node)
			if api.in_sky_realm(pos) then return end
			if ejectequiv(node.name, 1, pos) then
				return minetest.remove_node(pos)
			end
		end
	})
