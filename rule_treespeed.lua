-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local base = nodecore.tree_growth_rate
function nodecore.tree_growth_rate(pos)
	local qty = base(pos)
	if api.in_sky_realm(pos) then qty = qty * 10 end
	return qty
end
