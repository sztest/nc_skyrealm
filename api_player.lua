-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nodecore, pairs
    = math, minetest, nodecore, pairs
local math_cos, math_floor, math_pi, math_random
    = math.cos, math.floor, math.pi, math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local cache = {}

function api.playerdata(player)
	local pname = player:get_player_name()
	local data = cache[pname]
	if not data then
		data = player:get_meta():get_string(modname)
		data = data and data ~= "" and minetest.deserialize(data) or {}
		cache[pname] = data
	end
	return data, function()
		return player:get_meta():set_string(modname,
			minetest.serialize(data))
	end
end

local fademax = 4

local function setfade(player, fade)
	local txr = "[combine:16x16^[noalpha^[opacity:"
	.. math_floor((0.5 - 0.5 * math_cos(fade / fademax * math_pi)) * 16) * 16
	nodecore.hud_set(player, {
			label = modname .. " fade",
			hud_elem_type = "image",
			position = {x = 0.5, y = 0.5},
			text = txr,
			direction = 0,
			scale = {x = -100, y = -100},
			offset = {x = 0, y = 0}
		})
end

local function playereffects(pos, player)
	if not nodecore.player_visible(player) then return end
	api.teleportsound(pos)
	return api.particleburst({x = pos.x, y = pos.y + 1, z = pos.z},
		{x = 0.25, y = 0.75, z = 0.25}, player)
end

local lockpos = {}
local function player_move(player, pos)
	lockpos[player:get_player_name()] = pos
	player:set_pos(pos)
	player:set_look_horizontal(math_random() * 2 * math_pi)
end

function api.player_enter(player, pos)
	if api.in_sky_realm(player:get_pos()) then return end
	local data, save = api.playerdata(player)
	data.rtn = player:get_pos()
	data.fade = fademax
	setfade(player, fademax)
	nodecore.inventory_dump(player)
	nodecore.setphealth(player, 0)
	nodecore.sound_play("player_damage", {pos = data.rtn})
	playereffects(data.rtn, player)
	player_move(player, pos)
	nodecore.sound_play("player_damage", {pos = pos})
	playereffects(pos, player)
	return save()
end

function api.player_return_pos(data, ppos)
	return api.portaldata(data.laststand or ppos) or data.rtn or {x = 0, y = 256, z = 0}
end

function api.player_return(player)
	local ppos = player:get_pos()
	if not api.in_sky_realm(ppos) then return end
	local data, save = api.playerdata(player)
	data.fade = fademax
	setfade(player, fademax)
	nodecore.sound_play("player_damage", {pos = ppos})
	playereffects(ppos, player)
	local dest = api.player_return_pos(data, ppos)
	dest.keepinv = true
	player_move(player, dest)
	nodecore.sound_play("player_damage", {pos = dest})
	playereffects(dest, player)
	nodecore.inventory_dump(player)
	nodecore.setphealth(player, 0)
	return save()
end

local function scanup(pos)
	local node = minetest.get_node(pos)
	if node.name == "ignore" then return pos end
	if api.air[node.name] then
		pos.y = pos.y + 1
		node = minetest.get_node(pos)
		if node.name == "ignore" then return pos end
		if api.air[node.name] then return pos, true end
		return scanup(pos)
	end
	pos.y = pos.y + 1
	return scanup(pos)
end

local function checkplayer(player, dtime)
	local data, save = api.playerdata(player)
	if not data.fade then return end
	if data.fade == fademax then
		local pos, ok = scanup(lockpos[player:get_player_name()] or player:get_pos())
		player_move(player, pos)
		lockpos[player:get_player_name()] = (not ok) and pos or nil
		if ok then data.fade = data.fade - dtime end
	elseif data.fade <= 0 then
		nodecore.hud_set(player, {
				label = modname .. " fade",
				ttl = 0,
				quick = true
			})
		data.fade = nil
	elseif data.fade then
		setfade(player, data.fade)
		data.fade = data.fade - dtime
	end
	return save()
end
minetest.register_globalstep(function(dtime)
		for _, player in pairs(minetest.get_connected_players()) do
			checkplayer(player, dtime)
		end
	end)
