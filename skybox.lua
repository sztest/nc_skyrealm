-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, pairs
    = minetest, nodecore, pairs
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

nodecore.register_playerstep({
		label = "skyrealm skybox",
		priority = -100,
		action = function(player, data)
			if not api.in_sky_realm(player:get_pos()) then return end
			local oldsky = data.sky
			if not oldsky then return end
			local txr = oldsky.textures
			if not (txr and #txr >= 6) then return end
			local sky = {}
			for k, v in pairs(oldsky) do sky[k] = v end
			data.sky = sky
			sky.textures = {
				txr[2] .. "^[transformR180",
				txr[1] .. "^[transformR180",
				txr[4] .. "^[transformR180",
				txr[3] .. "^[transformR180",
				txr[6] .. "^[transformR180",
				txr[5] .. "^[transformR180"
			}
		end
	})
