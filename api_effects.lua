-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nodecore, pairs, vector
    = math, minetest, nodecore, pairs, vector
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

api.air = {air = true}
local function findair()
	for k, v in pairs(minetest.registered_nodes) do
		if v.air_equivalent or (v.groups and v.groups.air_equivalent
			and v.groups.air_equivalent > 0) then
			api.air[k] = true
		end
	end
end
minetest.register_on_mods_loaded(findair)
minetest.after(0, findair)

function api.particleburst(pos, size, thing)
	local mult = 1
	local qty = 50
	local def
	if not thing then
		return
	elseif thing.tiles or thing.inventory_image then
		def = thing
	elseif thing.get_player_name then
		def = {inventory_image = nodecore.player_skin(thing, {})}
		mult = 5
		qty = 20
	else
		return
	end
	for _ = 1, mult do
		local v = 0.05
		nodecore.digparticles(def, {
				time = 0.05,
				amount = qty,
				minpos = vector.subtract(pos, size),
				maxpos = vector.add(pos, size),
				minvel = {x = -v, y = -v, z = -v},
				maxvel = {x = v, y = v, z = v},
				minexptime = 0.5,
				maxexptime = 3,
				minsize = 8,
				maxsize = 12
			})
	end
end

function api.teleportsound(pos)
	for _ = 1, 5 do
		nodecore.sound_play("nc_skyrealm_teleport", {
				pos = {
					x = pos.x + math_random() * 4 - 2,
					y = pos.y + math_random() * 4 - 2,
					z = pos.z + math_random() * 4 - 2,
				},
				gain = 2,
				pitchvary = 0.5
			})
	end
end
