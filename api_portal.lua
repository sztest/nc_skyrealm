-- LUALOCALS < ---------------------------------------------------------
local minetest, vector
    = minetest, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]
local modstore = api.store

local storekey = "portals"
local s = modstore:get_string(storekey) or ""
local cache = s and s ~= "" and minetest.deserialize(s) or {}

function api.portaldata(skypos)
	local key = minetest.pos_to_string(api.island_grid_round(skypos))
	return cache[key], function(v)
		v = vector.round(v)
		if cache[key] and v and vector.equals(cache[key], v) then return end
		cache[key] = v
		return modstore:set_string(storekey, minetest.serialize(cache))
	end
end
