-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nodecore
    = math, minetest, nodecore
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

minetest.register_abm({
		label = "aggregate insta-cooking",
		interval = 1,
		chance = 1,
		nodenames = {"nc_concrete:aggregate_wet_source"},
		neighbors = {"group:lava"},
		action = function(pos)
			if not api.in_sky_realm(pos) then return end
			nodecore.smokeburst(pos)
			nodecore.dynamic_shade_add(pos, 1)
			if math_random() < 0.05 then
				nodecore.set_loud(pos, {name = "nc_lux:stone"})
				return nodecore.witness(pos, "skyrealm aggregate bake")
			end
			return nodecore.set_loud(pos, {name = "nc_terrain:stone"})
		end
	})

local skyhint = api.addskyhint()
skyhint("quick-dry aggregate into lux",
	"skyrealm aggregate bake",
	"nc_concrete:aggregate"
)
