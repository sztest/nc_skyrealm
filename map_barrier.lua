-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local nodename = modname .. ":barrier"

minetest.register_node(nodename, {
		description = "SkyRealm Barrier",
		drawtype = "airlike",
		walkable = false,
		climbable = false,
		pointable = false,
		buildable_to = false,
		paramtype = "light",
		sunlight_propagates = true
	})

local barrierid = minetest.get_content_id(nodename)
nodecore.register_mapgen_shared({
		label = "skyrealm barrier",
		func = function(minp, maxp, area, data)
			if minp.y > api.barrier_ymax then return end
			if maxp.y < api.barrier_ymin then return end
			local ymin = api.barrier_ymin
			if ymin < minp.y then ymin = minp.y end
			local ymax = api.barrier_ymax
			if ymax > maxp.y then ymax = maxp.y end
			for z = minp.z, maxp.z do
				for y = ymin, ymax do
					for x = minp.x, maxp.x do
						data[area:index(x, y, z)] = barrierid
					end
				end
			end
		end
	})

local function delents(self)
	local pos = self.object:get_pos()
	if not pos then return end
	if minetest.get_node(pos).name == nodename then
		self.object:remove()
		return true
	end
end
nodecore.register_falling_node_step(delents)
nodecore.register_item_entity_step(delents)
