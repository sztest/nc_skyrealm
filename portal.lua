-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nodecore, pairs, tostring, vector
    = math, minetest, nodecore, pairs, tostring, vector
local math_ceil, math_cos, math_pi, math_random
    = math.ceil, math.cos, math.pi, math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local portalname = modname .. ":portal"

local function mkisland(pos)
	pos = pos and api.in_sky_realm(pos) and pos
	local ipos = api.island_near(pos or {
			x = (math_random() * 2 - 1) * api.islands_xzextent,
			y = api.islands_ymin + (api.islands_ymax - api.islands_ymin) * math_random(),
			z = (math_random() * 2 - 1) * api.islands_xzextent
		})
	if ipos then return ipos end
	return mkisland()
end

local function getassign(meta, pos, noset)
	local found = meta:get_string(modname)
	if found ~= "" then return minetest.string_to_pos(found) end
	if noset then return end
	local ipos = mkisland(pos)
	meta:set_string(modname, minetest.pos_to_string(ipos))
	return ipos
end

local w = 16
local frames = 32
local base = nodecore.tmod(modname .. "_particle.png"):resize(w, w)
local txr = nodecore.tmod:combine(w, frames * w)
for i = 0, frames - 1 do
	local o = math_ceil((1 - math_cos(i * math_pi * 2 / frames)) * 128)
	if o > 255 then o = 255 end
	txr = txr:layer(0, w * i, base:opacity(o))
end
txr = tostring(txr)

local function portaltick(pos, att, stack)
	local insky = api.in_sky_realm(pos)
	local inert = insky or stack or att
	if not insky then
		local assign = getassign(stack and stack:get_meta()
			or minetest.get_meta(pos), nil, true)
		if assign then
			local _, set = api.portaldata(assign)
			set(pos)
			if api.return_falling then api.return_falling(pos, assign) end
		end
	end
	if att then
		pos = {x = 0, y = 0, z = 0}
		if att:is_player() then
			pos.y = pos.y + att:get_properties().eye_height
		end
	end
	return minetest.add_particlespawner({
			time = 1,
			amount = inert and 50 or 100,
			exptime = {
				min = 1,
				max = 5,
				bias = 1
			},
			size = {
				min = 0.2,
				max = inert and 1 or 2
			},
			minvel = vector.new(-0.1, -0.1, -0.1),
			maxvel = vector.new(0.1, 0.1, 0.1),
			minacc = vector.new(-0.1, -0.1, -0.1),
			maxacc = vector.new(0.1, 0.1, 0.1),
			texture = txr,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				length = -1
			},
			pos = pos,
			radius = {
				min = 1,
				max = inert and 2 or 8,
				bias = 1
			},
			attract = (not inert) and {
				kind = "point",
				strength = {
					min = 0.25,
					max = 2,
					bias = 1
				},
				origin = pos
			} or nil,
			collisiondetection = true,
			collision_removal = true,
			attached = att
		})
end
local function portaltickstart(pos)
	nodecore.dnt_reset(pos, modname .. ":porticles")
	return portaltick(pos)
end

local function touchtip(name, meta)
	return name .. "\n" .. nodecore.notranslate(api.islandhash(getassign(meta)))
end

minetest.register_node(portalname, {
		description = "SkyRealm Portal",
		drawtype = "nodebox",
		node_box = nodecore.fixedbox(
			{-5/16, -4/16, -3/16, 5/16, -2/16, 3/16},
			{-3/16, -4/16, -5/16, 3/16, -2/16, 5/16},
			{-3/16, -6/16, -3/16, 3/16, -2/16, 3/16},
			{-1/16, -7/16, -1/16, 1/16, 2/16, 1/16},
			{-5/16, 2/16, -3/16, 5/16, 6/16, 3/16},
			{-3/16, 2/16, -5/16, 3/16, 6/16, 5/16},
			{-3/16, 0/16, -3/16, 3/16, 8/16, 3/16}
		),
		tiles = {"[combine:1x1^[noalpha"},
		collision_box = nodecore.fixedbox(),
		selection_box = nodecore.fixedbox(-5/16, -7/16, -5/16, 5/16, 8/16, 5/16),
		stack_max = 1,
		groups = {cracky = 7},
		paramtype = "light",
		sounds = nodecore.sounds("nc_terrain_stony"),
		preserve_metadata = function(_, _, oldmeta, drops)
			drops[1]:get_meta():set_string(modname, oldmeta[modname])
		end,
		after_place_node = function(pos, _, itemstack)
			minetest.get_meta(pos):set_string(modname,
				itemstack:get_meta():get_string(modname))
		end,
		on_stack_touchtip = function(stack, name)
			return touchtip(name, stack:get_meta())
		end,
		on_node_touchtip = function(pos, _, name)
			return touchtip(name, minetest.get_meta(pos))
		end,
		on_construct = portaltickstart,
		mapcolor = {r = 0, g = 0, b = 0},
	})

nodecore.register_dnt({
		name = modname .. ":porticles",
		time = 1,
		loop = true,
		nodenames = {portalname},
		action = function(pos) return portaltick(pos) end
	})
nodecore.register_lbm({
		name = modname .. ":porticles",
		nodenames = {portalname},
		run_at_every_load = true,
		action = portaltickstart
	})
nodecore.register_aism({
		label = "SkyRealm Portal Stack FX",
		chance = 1,
		interval = 1,
		itemnames = {portalname},
		action = function(stack, data)
			portaltick(data.pos, data.player or data.obj, stack)
		end
	})

nodecore.register_craft({
		label = "skyrealm teleport",
		action = "pummel",
		duration = 2,
		touchgroups = {flame = 5},
		check = function(pos) return not api.in_sky_realm(pos) end,
		nodes = {
			{
				match = portalname
			}
		},
		after = function(pos, data)
			if not data.crafter then return end
			for _, p in pairs(nodecore.find_nodes_around(pos, "group:flame")) do
				nodecore.sound_play("nc_fire_snuff", {gain = 1, pos = p})
				minetest.remove_node(p)
			end
			for _, p in pairs(nodecore.find_nodes_around(pos, "group:ember", 2)) do
				nodecore.sound_play("nc_fire_snuff", {gain = 1, pos = p})
				nodecore.set_loud(p, {name = "nc_fire:ash"})
				nodecore.smokefx(p, 0.2, 50)
			end
			local ipos = getassign(minetest.get_meta(pos), pos)
			api.player_enter(data.crafter, ipos)
		end
	})

local function airscan(pos)
	local nn = minetest.get_node(pos).name
	if nn == "ignore" then return true end
	if not api.air[nn] then return end
	pos.y = pos.y - 1
	return airscan(pos)
end
local function scanaround(pos)
	return airscan({x = pos.x, y = pos.y, z = pos.z})
	and airscan({x = pos.x + 1, y = pos.y, z = pos.z})
	and airscan({x = pos.x - 1, y = pos.y, z = pos.z})
	and airscan({x = pos.x, y = pos.y, z = pos.z + 1})
	and airscan({x = pos.x, y = pos.y, z = pos.z - 1})
	and airscan({x = pos.x + 1, y = pos.y, z = pos.z + 1})
	and airscan({x = pos.x + 1, y = pos.y, z = pos.z - 1})
	and airscan({x = pos.x - 1, y = pos.y, z = pos.z + 1})
	and airscan({x = pos.x - 1, y = pos.y, z = pos.z - 1})
end
local function checkplayer(player, dtime)
	local pos = player:get_pos()
	if not api.in_sky_realm(pos) then return end
	local data, save = api.playerdata(player)
	local function complete()
		data.falling = nil
		save()
		nodecore.player_discover(player, "skyrealm return")
		return api.player_return(player)
	end
	local node = minetest.get_node(pos)
	if node.name == modname .. ":barrier" then complete() end
	local yvel = player:get_velocity().y
	if yvel > 0 or not scanaround(pos) then
		local rpos = vector.round(pos)
		if data.falling or not (data.laststand
			and vector.equals(data.laststand, rpos)) then
			data.laststand = rpos
			data.falling = nil
			return save()
		end
		return
	end
	local ipos = api.island_near(pos)
	if ipos and pos.y > ipos.y - 8 then return end
	data.falling = (data.falling or 0) + (yvel < 0 and dtime or 0)
	if data.falling >= 4 then return complete() end
	return save()
end
minetest.register_globalstep(function(dtime)
		for _, player in pairs(minetest.get_connected_players()) do
			checkplayer(player, dtime)
		end
	end)

nodecore.register_craft({
		label = "build skyrealm portal",
		action = "pummel",
		toolgroups = {thumpy = 6},
		norotate = true,
		nodes = {
			{
				match = "nc_lode:block_hot",
				replace = portalname
			},
			{
				y = -1,
				match = "nc_tree:root",
				replace = "nc_fire:ember5"
			},
			{
				y = 1,
				match = {groups = {lux_fluid = true}},
				replace = "nc_fire:ember1"
			},
			{
				x = 1,
				match = {groups = {lux_fluid = true}},
				replace = "nc_fire:ember1"
			},
			{
				x = -1,
				match = {groups = {lux_fluid = true}},
				replace = "nc_fire:ember1"
			},
			{
				z = 1,
				match = {groups = {lux_fluid = true}},
				replace = "nc_fire:ember1"
			},
			{
				z = -1,
				match = {groups = {lux_fluid = true}},
				replace = "nc_fire:ember1"
			}
		},
		after = function(pos) return getassign(minetest.get_meta(pos), pos) end
	})

local addhint = nodecore.register_hint or nodecore.addhint
addhint("create a skyrealm portal",
	"build skyrealm portal",
	{
		"toolcap:thumpy:6",
		"forge lode block",
		"nc_tree:root",
		"group:lux_cobble_max"
	})
addhint("enter the skyrealm",
	"skyrealm teleport",
	"build skyrealm portal"
)

local skyhint = api.addskyhint("goal")
skyhint("fall back to the surface world",
	"skyrealm return",
	"skyrealm teleport"
)
