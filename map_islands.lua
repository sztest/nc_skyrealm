-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local l1 = {
	".....",
	".....",
	".xs..",
	".....",
	".....",
}
local l2 = {
	".....",
	".ddd.",
	".dsd.",
	".ddd.",
	".....",
}
local l3 = {
	".ggg.",
	"ggggg",
	"gg.gg",
	"ggggg",
	".ggg.",
}

api.isle_schematic = nodecore.ezschematic(
	{
		["."] = {name = "air", prob = 0},
		s = {name = "nc_terrain:stone", prob = 255},
		d = {name = "nc_terrain:dirt", prob = 255},
		g = {name = "nc_terrain:dirt_with_grass", prob = 255},
		x = {name = modname .. ":treestarter", prob = 255},
	},
	{l1, l2, l3}
)

minetest.register_on_generated(function(minp, maxp)
		if maxp.y < api.islands_ymin then return end
		if minp.y > api.islands_ymax then return end
		local vm = minetest.get_mapgen_object("voxelmanip")
		local gmin = api.island_grid_round(minp)
		local gmax = api.island_grid_round(maxp, 1)
		for x = gmin.x, gmax.x, api.islands_grid do
			for y = gmin.y, gmax.y, api.islands_grid do
				for z = gmin.z, gmax.z, api.islands_grid do
					local pos = api.island_near({x = x, y = y, z = z})
					if pos and api.air[minetest.get_node(pos).name] then
						pos.x = pos.x - 2
						pos.z = pos.z - 2
						minetest.place_schematic_on_vmanip(vm, pos,
							nodecore.tree_schematic)
						pos.y = pos.y - 2
						minetest.place_schematic_on_vmanip(vm, pos,
							api.isle_schematic)
					end
				end
			end
		end
		vm:write_to_map()
	end)
