-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, nodecore, pairs
    = ipairs, minetest, nodecore, pairs
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local funckeys = {reqs = true}

function api.addskyhint(...)
	local mykeys = {}
	for k in pairs(funckeys) do mykeys[k] = true end
	for _, k in ipairs({...}) do mykeys[k] = nil end
	return function(text, ...)
		local hint = (nodecore.register_hint or nodecore.addhint)(text .. " [sky]", ...)
		for k in pairs(mykeys) do
			local old = hint[k]
			hint[k] = function(db, pname, player, ...)
				if not player then return end
				local pos = player:get_pos()
				if not (pos and api.in_sky_realm(pos)) then return end
				return old(db, pname, player, ...)
			end
		end
		return hint
	end
end
