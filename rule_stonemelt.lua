-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

nodecore.register_craft({
		label = "melt stone to lava",
		action = "cook",
		touchgroups = {flame = 4},
		duration = 30,
		cookfx = true,
		check = api.in_sky_realm,
		nodes = {
			{
				match = "nc_terrain:stone",
				replace = "nc_terrain:lava_source"
			}
		}
	})

nodecore.register_cook_abm({nodenames = {"nc_terrain:stone"}, neighbors = {"group:flame"}})

local skyhint = api.addskyhint()
skyhint("melt stone",
	"melt stone to lava",
	"nc_terrain:stone"
)
