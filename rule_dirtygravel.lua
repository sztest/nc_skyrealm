-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

minetest.register_node(modname .. ":dirty_gravel", {
		description = "Dirty Gravel",
		tiles = {"nc_terrain_gravel.png^(nc_terrain_dirt.png^[opacity:127)"},
		groups = {
			crumbly = 1,
			skyrealm_only = 1
		},
		alternate_loose = {
			groups = {
				crumbly = 2,
				falling_repose = 2
			}
		},
		skyrealm_equiv = {"nc_terrain:dirt_loose", "nc_terrain:gravel_loose"},
		sounds = nodecore.sounds("nc_terrain_swishy"),
		mapcolor = {r = 65, g = 65, b = 65},
	})

nodecore.register_craft{
	label = "cook sand from dirty gravel",
	action = "cook",
	touchgroups = {flame = 3},
	duration = 30,
	cookfx = true,
	check = api.in_sky_realm,
	nodes = {
		{
			match = modname .. ":dirty_gravel",
			replace = "nc_terrain:sand"
		}
	}
}

nodecore.register_cook_abm({nodenames = {modname .. ":dirty_gravel"}, neighbours = {"group:flame"}})

nodecore.register_craft{
	label = "mix dirty gravel (fail)",
	action = "pummel",
	toolgroups = {thumpy = 2},
	normal = {y = 1},
	priority = 2,
	check = api.in_sky_realm,
	nodes = {
		{
			match = {groups = {dirt = true}},
			replace = "air"
		},
		{
			y =-1,
			x = 1,
			match = {buildable_to = true},
			replace = "nc_terrain:gravel"
		},
		{
			y =-1,
			match = {groups = {gravel = true}},
			replace = "nc_terrain:dirt"
		}
	}
}

nodecore.register_craft{
	label = "mix dirty gravel",
	action = "pummel",
	toolgroups = {thumpy = 2},
	normal = {y = 1},
	priority = 1,
	check = api.in_sky_realm,
	nodes = {
		{
			match = {groups = {dirt = true}},
			replace = "air"
		},
		{
			y =-1,
			match = {groups = {gravel = true}},
			replace = modname .. ":dirty_gravel"
		}
	}
}

local skyhint = api.addskyhint()
skyhint("mix dirty gravel",
	"mix dirty gravel",
	{"group:dirt", "group:gravel"}
)
skyhint("cook sand from dirty gravel",
	"cook sand from dirty gravel",
	"mix dirty gravel"
)
