-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

nodecore.register_craft({
		label = "break amalgamation to lode prills",
		action = "pummel",
		check = api.in_sky_realm,
		nodes = {
			{
				match = {groups = {amalgam = true}},
				replace = "nc_terrain:gravel"
			},
		},
		items = {
			{name = "nc_lode:prill_hot", scatter = 5}
		},
		toolgroups = {cracky = 2},
		itemscatter = 5
	})

local skyhint = api.addskyhint()
skyhint("pick lode from amalgamation",
	"break amalgamation to lode prills",
	{"group:amalgam", "toolcap:cracky:2"}
)
